package com.example.khaicoder.multipleasystask;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Adapterpicture extends RecyclerView.Adapter<Adapterpicture.Viewholder> {
private IGetItems iGet;

    public Adapterpicture(IGetItems iGet) {
        this.iGet = iGet;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items, viewGroup, false);
        return new Viewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int i) {
        Picture picture = iGet.getItems(i);
        Context context = viewholder.img.getContext();
        Picasso.with(context).load(picture.getPath()).into(viewholder.img);
    }

    @Override
    public int getItemCount() {
        return iGet.getCount();
    }

    class Viewholder extends RecyclerView.ViewHolder {
        private ImageView img;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img);
        }
    }

    public interface IGetItems{
        int getCount();
        Picture getItems(int position);
    }
}
