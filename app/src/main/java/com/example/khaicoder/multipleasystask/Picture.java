package com.example.khaicoder.multipleasystask;

import android.graphics.Bitmap;

import java.io.File;

public class Picture {
    private File path;
private String name;

    public Picture(File path, String name) {
        this.path = path;
        this.name = name;
    }

    public File getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(File path) {
        this.path = path;
    }
}
