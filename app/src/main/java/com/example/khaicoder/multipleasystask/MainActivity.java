package com.example.khaicoder.multipleasystask;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements Adapterpicture.IGetItems {
    private static String TAG = MainActivity.class.getSimpleName();
    private ImageView imageView;
    private String url1, url2, url3, url4, url5;
    private List<Picture> list = new ArrayList<>();
    private Adapterpicture adapterpicture;
    private RecyclerView rc;
    private Bitmap bitmap;


    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initsUrl();
        rc = findViewById(R.id.rc_view);
        rc.setLayoutManager(new LinearLayoutManager(this));
        adapterpicture = new Adapterpicture(this);

        execute();

        rc.setAdapter(adapterpicture);

    }

//    private void runAsynctask() {
//
//
//        adapterpicture.notifyDataSetChanged();
////        new FetchItemsTask(url1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////        new FetchItemsTask(url2).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////        new FetchItemsTask(url3).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////        new FetchItemsTask(url4).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////        new FetchItemsTask(url5).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//    }

    private void initsUrl() {
        url1 = "https://hinhanhdephd.com/wp-content/uploads/2016/03/hinh-anh-meo-con-de-thuong-nhat-1.jpg";
        url2 = "http://file.vforum.vn/hinh/2015/12/hinh-anh-meo-de-thuong-nhat-qua-dat-2.jpg";
        url3 = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTA-V5GMjSIZLL0x5_Pn5wrXFs__BwR0CKounN6q4g5NiVdMGnt";
        url4 = "https://i.ytimg.com/vi/V9xlD4Hpm88/maxresdefault.jpg";
        url5 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHgAtgMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAGAAMEBQECBwj/xAA4EAACAQMDAgQDBgUEAwEAAAABAgMABBEFEiExQQYTIlFhcYEHFDJCkbEjUqHh8BViwdEWgrIz/8QAGQEAAgMBAAAAAAAAAAAAAAAAAQMAAgQF/8QAIREAAgICAgIDAQAAAAAAAAAAAAECEQMhEjETQSIyUQT/2gAMAwEAAhEDEQA/AJErhe9V9xKK1muB71AnuBihGIszK3NRmPNNvcc1qJQa1QiKbHhzW2KbRhTmQaZxF2aMKaIp1gW4HWnYtOupyBFGWB7jp+tDiFSRCOAaxnNWv+jCENJqN1Hbopw231H/ADisFNEtIY5rx7r1/giVgZH+mOPrRqgcreiDFFuIHvVxY6aGGcVvbw6fLLF5ZaxZ+kd1Kpz9R0+tE1nZmLaG2MCMhkYMD8iKXksZBpsrYNLAHSp8FgF7VdwW4IHFShbD2rI2aEVdvBsxxU+MYpwwgUlSq0Ws2WtwtZVacVagDRU5p5UpBacUcUaIYEdZ24pwGtiKJBnFYp3FKoA4PPd89ahyXBY1DaYsetZU1thBITKQ6ZSa2WQ96jseawNx5J9I600XVk9JuQB1p2W6EGARuduiiqua5VQFhXbxye9XvhvSTczPd3GcQqGA+J6UG6BxH0iX7vNc3Y2Qx4yASASe2e5+FNXHiIRq62sjZcbWc9lHRR7CqzxrqUxNtpURX+Bl5QgwAx6fXB/ahYSXGSDkUlytDseFdsMrfV1H48Ng5yTnn61IOsRyECQIwJz6lBNAwaYsAW61mczRSKAx6UtpMelXQZPFp1zJvkhi3EjJUYP96ubPXri2QQpHavAOUwm11Hw5x+1c2W6uI8+rlaeGoXfl5HH1oq17Kygp/ZHV9O8cABoVspEKdC8u4t/1RHpviiC42pOoifPR+OfhXDbPX5oLIodm8E4JXPX3zVzoviyx8zGtQSPg5jkgI4wOBtPX9RTKg1tGeWPIvozttxqNpAyrPJ5ZJ25KkjOM9RxTsEsU674JFkX3U5rmGj+MNKa49UrGAeiOKZvWB759zRRbPGY/vOlGc7iCxVOmffHUfGqvAn0V884v5IL1WnFWqvQ9VF43kzjE3vjGcdfrV4ExWaUHF0zTGSkrQ0FrbbToWltoFhrFYORTu2sFagBvJpVvtNKoQ80IlSogucU0rcVgOE+ddIzPZKdY1XPWos7ZTjgVqz5PJrI9QI7GoRaJHh3S31XVooAP4e71n4UYSazHDYy/dYBGWlKwMed2OBn4cGmtI0+bT9AuREfLvnRJF2/iKE+oj6ftQ7ql7HJfrbxsPJt/Qoz0I70jK9UMwxU3Zi70mQ3S3T5YyYZ93PJ61pc6WqybQOoyOKJfMS5t4mBH4cNjtTbQiUKCORXNlldnShBARe2Txsu1fzYFZurbzYVdRk0avpAmhcFfUFPSqWTTZrIoZEJRgd3wzn+1MhmtFZQpgq0R3bSMHFTWtSlipYck8VPktEN2BjAZ1K/EHBq1m083kwt419MbYLAfL+9NlkoooAtHphePcy08mjDb+DmjVtFEdv0yc9q0WzUDJH9KV52X8SAO5057dWk5CipPhzXdR0y5EltfXEOBjCucAfLpV/qsSCFwFDZ9+1Bs22G4ITnJ6CtWObktmfJBJnbvD+rz61saV4W1GIk70IRpfYlc+3eukW7edCkmCNwyQRgg/GvOEKxrbWt8rygiNR5kJ2nqR1+BBH6V1/wN4z0++gWzubiWK4BG0XJBL9uGHWrZE5Rv8Myisc3+MNNtLZTnxHSsFgOpA+tZx1mmwUtorfr0pVAjZQUq3NKgQ8ubsCmGkyayX9NRi3qromclkkgY7VO0KI3l/FCoyS3IqujbIoq+zm1WbWnZ8hdpXdjOOD/ao2VlpF/4isHi1pZUAWOKy3YBOAVHNc2gjaRw/JbOW+NdO0LT5o7DVDdO7N5ciDPOenT/ADvQLYwJFjPQVkyyro1fzw0kyz0oyKChGFNX8EDqo3jhuQ3tVfpl1ZFwjyqM8c0XWvkxxbXZNmM4Jrm5LbN6aXQxptqbpHXB3KDxS0H7vrKz28sYLw5R1I6EGnYPFujWtyI4be6lwdvmRR5GfhnrVn4asbC41K91nSbgS21z+OIqVaOUdQQelW8bStinNNgVfeGmtb+zjAJVMKc9eGOP6ftV6dPi0PQ3vJhlhl2OOSSeKJbiJbi7RyAcHk4rbxPp9rdaAVvblba0EitJIRkkBugA6knA+tCPKTphbSQLLA50+CaQYeZd2Ph71V3cDAEKKJp9e0+S2eW10u6ls7bCyTZUBB2OCc/0rSV9OmtjPDInlkZ3E0ZQcSRmpdHMNYR2ZlXOOhoZkgJuVjjH8TPA70aarc2k0k625ztzg1XeHdLF1d+c49RbgntWvFJpbF5Ui18P6HMfD/k+V5zwMXMRHMisASP1GR8RUWIXOn3MUtuFIHKSiMDPboe/XIrolhbRwgKqJuA6EH29x2qt1yGBFebyVEjn1PnG7njIFNhlpmaaUtDmleNtWmRobmeIEen+GoLf8474rGq+Jr0BhYqWVUCtNNy0mPf9/wDORUXjmTaxxjoAMU892AApG5TwwParOdvoX4l72WNr4+1O0mVWfcrcsCm4t789B9BXTfC2vQa/p/3iJWR1O11YYOa4fqalTviOMcqRV99n/iaS01aGCWUmGZghyOAajipRtArg9dM7XilW5pVnHnkrfxTLHBrRZMikxreKofjk+NHXhmG8i8MXV5YMUn89NzL1CHiueK2CK6x4fHlaTpVoAwM8TPLlTgx4XHbHX96DdIpJW0ggtl+7aWsTHzH2ZfdyWz7+9cl+8odyJjcCQQK6ldXablRWcqxBZmPYHj9q5GiKdZuCqKUMzEbumM1mq9mmEthFoVpuw3l5yeQ3eju30SG7tGhTdHvQ7ULcbscUP6UsexVjhw+ONpoqtmkEAXDxuOc45Fc/I3ys2r60c78ib7xFH95+6XdlIu9ScMGU9Rn5ZzRJ4H1wr4wuMzmVb1MSufzOvIY/1FW+qyaRfzY1jTFuZwMCQDDH5kdaHr37guoxf6RaSWsMOM+WCT7dafL+lONUIh/M+XK9HRrch7osG9JYn6Zqk+0nUfu1poyFS9usrPOAemFx+7ZqRoUlxcwCbgKB0xTF/GzarDcTxPLbIPUm3Iyf+OOaywy8HbHSw89AhYLaRXcz6VcG+uLsgQ20RJBPQZ9gO+a6BBocNrpdtZyIk0kECpI3uwHNaWP3iW5KWFlFZwt1eFApYfOiCK0EEG0/XNPyZXlXQiGNYX2ck8U6HBaRyzwQCNj7GoHhuQRW6dQwPWjfxm9qbWaORlJxwVoJ0rEUfoIamYm+Ow5aDjTp1I3Enge/WhbxHqwmuGgBG1T2pnUNaa0tTHG38RutDEs5di5OSeSafGJnolySeofvUpQskW7PPeqlpcrzUrT7kISH5U9qZQDdrxc+VJxxgZrGl3Vvb6lbh4h6ZQd31p2e3tr2BljbEw5Bqlty738Ee31lwhA+eKtGtgkvieoom3RIw6EA0qzaR7baJT2QD+lKspc8djcOx/Stw1GVx4eeMYMePpVVcaQy59BFallTI4Moia61YT7dJs7orwkTRBs9A21gBx865jNZOhyAaOLRZNR8IW8dlN/GijCyQ5xuK8ZFWdSiJlaaZE1fXWAkS3YZ7sKFLO6Czs7HkmpF5b3EAYXETKe2RVISdxxVErQ5aOg6Hq6JKu5sn50dafrULqo9JU/KuG2ty0TDaTn3om07VpUXyEkKv13/AMh+Hx9v8NZp4d2O8iaOxxwR3qldqpnHOAW/rTumeHoVaRJXleJhgbwAQfcEcVzfQvEM8c3mKxW2iUFt/UnAPPx9/oKNNJ8d2ko23CrEx6Nng0l4dhWVroeFnq2j6jHp9ou+2uSfJlIBCHGW3e31oxGlQ/cjA+6Ut+JiRk/9ULf+Z+H3ulRr5S6/m/KPrUm68b6Xaxgw3UchPAweKPjX4SWVsIpTbaZbZLKg5IDHNCGq+LWO9LVcuAcADO7A7Y4Jx260NeLfEvnzQSyymSwuMFSclYz9Og68jlSO44I9dS3FteLIkvmocNFKOfMj6lHxwXAO5GHXt1IpkcWhLm7Imq+IZdavN4IWM/y96zHMkUe/fgjtUbW0gjnN5ZhQkjYlVRwkhAbj4EEEfWqS5u5H9Kg4p8YKtEcrJV5etcTEk960STvmosNtcS8pEzfIVh2aFisgKkdiMVcrZMZ/jSSYqwwajIzyfgUmnre2eS4iR+NzDioEJNNhEGmT30o4x6Ksvsm0qLVfEElxcpuFuAwyONxNRPE261s7bTLQF3kA9CjJJ9qMvsk0q70qC8kvYWilaRcKfbFLb+NlG0dWCjAHtWajLeA9qVKLnLtVs5LSd7edcOnw61Q3ECtncoNdh8RaJHqsG5MLOn4W9/ga5zqmkXNm5EkLbc9cVXoYnYJz6WkhO1Kb0gf6RqGJZDHBIcFx1jbs3y96u/MMZ6DHuKj30FvfREOdrDuKvHI0yThGSpiu4oplKTRRurEDz0cFX68gE57dMUI6p4dSQtLZA468VbpYXFqcRyRzxfyS9R9fb4ftU2a2ExjljuJrGVBtCiIuGGB3BwOnf3p9Re4sSpSjqaOdyWU9vIN6Hj4U5EJSMKp4PX3PvR1btG04gubqCVghKwmMhiOcdgPbscVtBpMV5Gzx2MluwGTvIRfpnrRfJLYYyi3oG5ZnOnRrEfWdplA/N15/+c/SmVaTaSdwyOKvpLKOCV4bho4XHqyzrjgfA+xqXp2nrqCDy438ssYxLt9JYDOB71Rb9FrUfYMRwNMv+6nE0+43BS5x3HvRkPDKR7MXsW5n2gAH9flT6PpOmXJhbzJb2JmRoduWLA4GO2PjQt+kDmn7KLTtFvJ7CS2ZJHRSWUMOmev9Qp/WrvSvDlxHCsNwyiEH0HOdnOcfLPP1PuaktqrPdYl2iMdIoiWeRsduBxV7ommajdxZW0mXkHdcOI0z8AMk/oKLUl2K5p9FD/4lZlDHNdeltoZQP5RgfoBinoPB2hWzbpXZ+43sBRovglLlC1xdlZDz6FyB9Sag3/2dzPAfu97DJKOglRlUj2OCapyd0XStW9FWsGh2qL5KQgr0ANOb9EuJkiayhkZ/xARg04ngC9ijj82OOV/zeS+0fLLc/XFEFj4Wnt4wInitAQA3lJvY/wDsf+qNoW7Bi88G2Usiy2ka2sR/EGXH6CtJbXw3pVwiixE1woJMr47UXv4YlbeX1KY7hj/8xxUBPAFk0m+4u7iY+zYANFOPtlWsnoodLu7XU9U32Wjo80J9M+OF+Zo0sYFgjYykvI7bmOeBWYtK+6II7fYiLwAq4rSQSR8MP0qkpX0MhCtsmKydlpVFSTilVRhfxuK1mgjmHrUGlSqEKHU/CGl3rF/KaGU/niOP1HSh+b7PCZMpeBkPcptP70qVQNsyPs5gI/iXlxn/AGqtSbbwHZQ48y5uZPgQo/YUqVQlsIrDTreyXbbKFHy5P1qekYxg8/PmlSogFNZWlwMT2sEgx+aMGhrVvs80bUJkljM1sUQIkcbfw1APQL2+mKVKipNdAaTKmf7Obtrp5hrKSI7MzBoCpJbO4ZDY5ye1R7b7PdTinmlkvbMtOxMkqIfMIJ6At0HJ6e/yrNKj5JfoOEfwKNG8L2WnesRBpj+JzyT9aIEhVQABis0qq9ljfArOKVKgQ1K1gcUqVQhqxFNHFKlRINtkVqYkYcilSoEGWhjz0FZpUqhD/9k=";

    }

    @Override
    public int getCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override
    public Picture getItems(int position) {
        return list.get(position);
    }


    //
//    private class FetchItemsTask extends AsyncTask<String, Void, Void> {
//        private String url;
//
//        public FetchItemsTask(String url) {
//            this.url = url;
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            new DownloadPicture().getUrlBytes(url);
//            loadImage();
//            return null;
//        }
//
//
//        @Override
//        protected void onPostExecute(Void imageBytes) {
//            adapterpicture.notifyDataSetChanged();
//        }
//    }


    private void execute() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        Runnable task1 = () -> {
            Log.d(TAG, "task1: ");

            new DownloadPicture().getUrlBytes(url1);
            loadImage();


        };
        Runnable task2 = () -> {

            Log.d(TAG, "task2: ");

            new DownloadPicture().getUrlBytes(url2);
            loadImage();


        };
        Runnable task3 = () -> {

            Log.d(TAG, "task3: ");
            new DownloadPicture().getUrlBytes(url3);

        };
        Runnable task4 = () -> {

            Log.d(TAG, "task4: ");
            new DownloadPicture().getUrlBytes(url4);
            loadImage();

        };
        Runnable task5 = () -> {

            Log.d(TAG, "task5: ");

            new DownloadPicture().getUrlBytes(url5);
            loadImage();


        };



        executorService.submit(task1);
        executorService.submit(task2);
        executorService.submit(task3);
        executorService.submit(task4);
        executorService.submit(task5);
//           executorService.submit(task2);
        adapterpicture.notifyDataSetChanged();
    }


    private void loadImage() {
        File fileName = new File(Environment.getExternalStorageDirectory(), "/dirr");
        if (fileName.listFiles() == null) {
            return;
        }
        for (File f : fileName.listFiles()
                ) {

            file = new File(Environment.getExternalStorageDirectory(), "/dirr/" + f.getName());

            Log.d(TAG, "loadImage: " + f.getPath());
            list.add(new Picture(new File(f.getPath()), f.getName()));
            for (int i = 0; i < list.size(); i++) {
                for (int j = list.size() - 1; j > i; j--) {
                    if (list.get(j).getName().equals(list.get(i).getName())) {
                        list.remove(j);
                    }
                }
            }

        }

    }

}