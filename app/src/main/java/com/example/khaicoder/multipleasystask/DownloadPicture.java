package com.example.khaicoder.multipleasystask;

import android.app.DownloadManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class DownloadPicture {

    void getUrlBytes(String urlSpec) {
        try {
             download(urlSpec);
        } catch (Exception e) {
            //e.printStackTrace();
            Log.e("UrlDownload", "error downloading");


        }

    }

    public void download(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/dirr");
            if (!file.isDirectory()) {
                file.mkdir();
            }

            File f = new File(Environment.getExternalStorageDirectory() + "/dirr",urlSpec+".png");

            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return ;
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            FileOutputStream fileOutputStream = new FileOutputStream(f);;
            while ((bytesRead = in.read(buffer))!=-1) {

                fileOutputStream.write(buffer,0,bytesRead);
            }
            fileOutputStream.close();


        } finally {
            connection.disconnect();
        }
    }

}
